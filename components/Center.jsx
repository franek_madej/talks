import React from "react";

export const Center = ({ style, children, ...props }) => (
  <div
    style={{
      height: "100%",
      width: "100%",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      flexDirection: "column",
      ...style,
    }}
    {...props}
  >
    {children}
  </div>
);
