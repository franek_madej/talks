import React from "react";
import { render } from "react-dom";

import { MDXProvider } from "@mdx-js/react";

import {
  Deck,
  FlexBox,
  Slide,
  Box,
  // Progress,
  FullScreen,
  Notes,
  Text,
  mdxComponentMap,
} from "spectacle";
import slides, { notes } from "../ci-cd-for-quick-code-iterations.mdx";

const theme = {
  size: {
    width: 1366,
    height: 768,
    maxCodePaneHeight: 200,
  },
  colors: {
    primary: "#ebe5da",
    secondary: "#ffee88",
    tertiary: "#282828",
    quaternary: "#ffc951",
    quinary: "#8bddfd",
  },
  fonts: {
    header: "Helvetica, Arial, sans-serif",
    text: "Helvetica, Arial, sans-serif",
    monospace: '"Consolas", "Menlo", monospace',
  },
  fontSizes: {
    h1: "72px",
    h2: "64px",
    h3: "56px",
    text: "44px",
    monospace: "44px",
  },
  space: [16, 24, 48],
};

const template = () => (
  <FlexBox
    justifyContent="space-between"
    position="absolute"
    height="20px"
    bottom={0}
    width={1}
  >
    <Box padding="0 1em">
      <FullScreen />
    </Box>
    <FlexBox
      alignItems="center"
      justifyContent="center"
      color="rgba(255, 255, 255, 0.25)"
      style={{ height: "20px" }}
    >
      <svg
        style={{
          fill: "rgba(255, 255, 255, 0.25)",
          height: "14px",
          width: "14px",
        }}
        viewBox="0 0 248 248"
      >
        <path d="m221.95 51.29c0.15 2.17 0.15 4.34 0.15 6.53 0 66.73-50.8 143.69-143.69 143.69v-0.04c-27.44 0.04-54.31-7.82-77.41-22.64 3.99 0.48 8 0.72 12.02 0.73 22.74 0.02 44.83-7.61 62.72-21.66-21.61-0.41-40.56-14.5-47.18-35.07 7.57 1.46 15.37 1.16 22.8-0.87-23.56-4.76-40.51-25.46-40.51-49.5v-0.64c7.02 3.91 14.88 6.08 22.92 6.32-22.19-14.83-29.03-44.35-15.63-67.43 25.64 31.55 63.47 50.73 104.08 52.76-4.07-17.54 1.49-35.92 14.61-48.25 20.34-19.12 52.33-18.14 71.45 2.19 11.31-2.23 22.15-6.38 32.07-12.26-3.77 11.69-11.66 21.62-22.2 27.93 10.01-1.18 19.79-3.86 29-7.95-6.78 10.16-15.32 19.01-25.2 26.16z" />
      </svg>

      <Text
        style={{ margin: "0px", padding: "0px", paddingLeft: "4px" }}
        fontWeight="100"
        fontSize="18px"
        lineHeight="18px"
        height="18px"
        color="inherit"
      >
        @franek_madej | alergeek.ventures
      </Text>
    </FlexBox>
    <Box padding="1em">{/*   <Progress />*/}</Box>
  </FlexBox>
);

const originalH1 = mdxComponentMap.h1;

mdxComponentMap.h1 = (props) =>
  originalH1({
    style: {
      color: "transparent",
      background: "linear-gradient(to right, #ffee88, #dd1155)",
      backgroundClip: "text",
    },
    ...props,
  });

const Presentation = () => (
  <MDXProvider components={mdxComponentMap}>
    <Deck loop theme={theme} template={template}>
      {slides
        .map((MDXSlide, i) => [MDXSlide, notes[i]])
        .map(([MDXSlide, MDXNote], i) => (
          <Slide key={`slide-${i}`} slideNum={i}>
            <MDXSlide />
            <Notes>
              <MDXNote />
            </Notes>
          </Slide>
        ))}
    </Deck>
  </MDXProvider>
);

render(<Presentation />, document.getElementById("root"));
